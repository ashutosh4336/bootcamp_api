const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Bootcamp = require('../models/Bootcamp');

// @desc        Get all Bootcaps
// @route       Get /api/v1/bootcamps
// @access      Public
// Get all BootCamps
exports.getBootcamps = asyncHandler(async (req, res, next) => {
  const bootcamps = await Bootcamp.find();
  res
    .status(200)
    .json({ success: true, count: bootcamps.length, data: bootcamps });
});

// @desc        Get Single Bootcamp
// @route       Get /api/v1/bootcamps/:id
// @access      Public
// Get a Single BootCamp
exports.getBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);

  if (!bootcamp) {
    return new ErrorResponse(
      `Bootcamp Not Found with id of ${req.params.id}`,
      404
    );
  }

  res.status(200).json({ success: true, data: bootcamp });
});

// @desc        Create New Bootcamp
// @route       POST /api/v1/bootcamps/:id
// @access      Public
// Create a Bootcamp
exports.createBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.create(req.body);
  res.status(201).json({
    sucess: true,
    data: bootcamp
  });
});

// @desc        Update Bootcamp
// @route       PUT /api/v1/bootcamps/:id
// @access      Private
// Update a BootCamp
exports.updateBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });
  if (!bootcamp) {
    return new ErrorResponse(
      `Bootcamp Not Found with id of ${req.params.id}`,
      404
    );
  }
  res.status(200).json({ sucess: true, data: bootcamp });
});

// @desc        Delete a Bootcamp
// @route       Delete /api/v1/bootcamps/:id
// @access      Private
// Delete a BootCamp
exports.deleteBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findByIdAndRemove(req.params.id);
  if (!bootcamp) {
    return new ErrorResponse(
      `Bootcamp Not Found with id of ${req.params.id}`,
      404
    );
  }
  res.status(200).json({ sucess: true, data: {} });
});
