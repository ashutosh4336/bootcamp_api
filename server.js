const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const errorHandler = require('./middleware/error');
// const logger = require('./middleware/logger');

//Load DotEnv File
dotenv.config({ path: './config/config.env' });
// Connect DB
const connectDB = require('./config/db');
connectDB();
//Route Files
const bootcamps = require('./routes/bootcamps');

const app = express();

//Body-Parser
app.use(express.json());

// Dev loggin ENV

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Mount Routes
app.use('/api/v1/bootcamps', bootcamps);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(PORT, () =>
  console.log(
    `server satred in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
);

// Handle Unhandled Promise Rejection
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red.bold);
  //Close Server & Exit
  server.close(() => process.exit(1));
});
