const ErrorResponse = require('../utils/errorResponse');

const errorHandler = (err, req, res, next) => {
  let error = { ...err }; //spread operator

  error.message = err.message;
  //Log to Console
  console.log(err);

  //Mongoose Bad ObjectID
  if (err.name === 'CastError') {
    const message = `Resource Not Found with id of ${err.value}`;
    error = new ErrorResponse(message, 404);
  }

  // Moongoose duplicate
  if (err.code === 11000) {
    const message = 'Duplicate Field Value Entered';
    error = new ErrorResponse(message, 400);
  }

  // Mongoose validation error
  if (err.name === 'ValidationError') {
    const message = Object.values(err.errors).map(val => val.message);
    error = new ErrorResponse(message, 400);
  }

  // console.log(err.name);
  res.status(error.statusCode || 500).json({
    sucess: false,
    error: error.message || 'Server Error'
  });
};

module.exports = errorHandler;
